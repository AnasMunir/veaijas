import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { NativeStorage } from 'ionic-native';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/Rx';


@Injectable()
export class Maanserver {
  userId: any;

  constructor(public http: Http) {
    console.log('Hello Maanserver Provider');
    NativeStorage.getItem('user').then(data => {
      this.userId = data.userId;
    })
  }

  sendIndividualDataToServer(signupData: any, userType) {
    console.log('Logging object from provider');
    console.log('signupData');

    let maanURL = 'http://tvtime.pk/vezejas/user/register_auth';
    let body = new URLSearchParams(signupData);
    body.set('userType', userType);
    body.set('firstName', signupData.firstName);
    body.set('lastName', signupData.lastName);
    body.set('phone', signupData.phone);
    body.set('address', signupData.address);
    body.set('email', signupData.email);
    body.set('password', signupData.password);
    // let headers = new Headers({'Access-Control-Allow-Origin': '*,*', 'Access-Control-Allow-Methods': '*,*'});
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    let options = new RequestOptions({ headers: headers });
    // let body = JSON.stringify(signupData);
    // let body = JSON.stringify({username: 'anas'});
    console.log(body);

    return this.http.post(maanURL, body.toString(), options)
      .map((res: any) => res.json())
      //...errors if any
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
  sendCompanyDataToServer(signupData: any, userType) {
    console.log('Logging object from provider');
    console.log('signupData');

    let maanURL = 'http://tvtime.pk/vezejas/user/register_auth';
    let body = new URLSearchParams(signupData);
    body.set('userType', userType);
    body.set('companyName', signupData.companyName);
    body.set('companyCode', signupData.companyCode);
    body.set('mvaCode', signupData.mvaCode);
    body.set('address', signupData.address);
    body.set('phone', signupData.phone);
    body.set('email', signupData.email);
    body.set('password', signupData.password);
    // let headers = new Headers({'Access-Control-Allow-Origin': '*,*', 'Access-Control-Allow-Methods': '*,*'});
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    let options = new RequestOptions({ headers: headers });
    // let body = JSON.stringify(signupData);
    // let body = JSON.stringify({username: 'anas'});
    console.log(body);

    return this.http.post(maanURL, body.toString(), options)
      .map((res: any) => res.json())
      //...errors if any
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
  loginDataToServer(loginData: any) {
    console.log('Logging object from provider');
    console.log('loginData');

    let maanURL = 'http://tvtime.pk/vezejas/user/login_auth';
    let body = new URLSearchParams(loginData);

    body.set('userId', loginData.id);
    body.set('password', loginData.password);
    // let headers = new Headers({'Access-Control-Allow-Origin': '*,*', 'Access-Control-Allow-Methods': '*,*'});
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    let options = new RequestOptions({ headers: headers });
    // let body = JSON.stringify(signupData);
    // let body = JSON.stringify({username: 'anas'});
    console.log(body);

    return this.http.post(maanURL, body.toString(), options)
      .map((res: any) => res.json())
      //...errors if any
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
  sendingFormsData(formsData: any, formsType: any, userId?: any) {
    console.log('Logging object from provider');

    let maanURL = 'http://tvtime.pk/vezejas/data/forms';
    let body = new URLSearchParams();

    let jsonData = JSON.stringify(formsData);

    body.set('userId', userId);
    body.set('formType', formsType)
    body.set('data', jsonData);
    // let headers = new Headers({'Access-Control-Allow-Origin': '*,*', 'Access-Control-Allow-Methods': '*,*'});
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    let options = new RequestOptions({ headers: headers });
    // let body = JSON.stringify(signupData);
    // let body = JSON.stringify({username: 'anas'});
    console.log(body);

    return this.http.post(maanURL, body.toString(), options)
      .map((res: any) => res.json())
      //...errors if any
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  sendingDeliveryData(sendersData: any, recipientData: any, userId?: any, deliveryId?: any, pickUpDate?: any) {
    console.log('Logging object from provider');

    let maanURL = 'http://tvtime.pk/vezejas/data/delivery';
    let body = new URLSearchParams();

    let jsonSender = JSON.stringify(sendersData);
    let jsonRecipient = JSON.stringify(recipientData);
    let data = jsonSender +'||'+jsonRecipient +'||'+ 'pickUpDate: ' + pickUpDate;

    body.set('userId', userId);
    body.set('deliveryId', deliveryId);
    body.set('data', data);
    // let headers = new Headers({'Access-Control-Allow-Origin': '*,*', 'Access-Control-Allow-Methods': '*,*'});
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    let options = new RequestOptions({ headers: headers });
    // let body = JSON.stringify(signupData);
    // let body = JSON.stringify({username: 'anas'});
    console.log(body);

    return this.http.post(maanURL, body.toString(), options)
      .map((res: any) => res.json())
      //...errors if any
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
  getNewsData() {
    let maanURL = 'http://tvtime.pk/vezejas/data/news'
    return this.http.get(maanURL)
      .map((res: any) => res.json())
      //...errors if any
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
  /*testingserver() {
    console.log('Logging object from provider tester');
    let testData: any = {
      height: "8",
      length:"5",
      moreInfo:"",
      weight:"5",
      width:"5"
    }
    console.log(testData);
    let jsonTest = JSON.stringify(testData);
    console.log(jsonTest);
    let maanURL = 'http://tvtime.pk/vezejas/data/forms';
    let body = new URLSearchParams(jsonTest);

    body.set('userId', '10009');
    body.set('formType', 'cargo')
    body.set('data', testData);
    // let headers = new Headers({'Access-Control-Allow-Origin': '*,*', 'Access-Control-Allow-Methods': '*,*'});
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    let options = new RequestOptions({ headers: headers });
    // let body = JSON.stringify(signupData);
    // let body = JSON.stringify({username: 'anas'});
    console.log(body);

    return this.http.post(maanURL, body.toString(), options)
      .map((res: any) => res.json())
      //...errors if any
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }*/
}
