import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, LoadingController, AlertController } from 'ionic-angular';

import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Maanserver } from '../../providers/maanserver';
import { DatePicker } from 'ionic-native';
import { TermsConditionsPage } from '../terms-conditions-page/terms-conditions-page';
import { MenuPage } from '../menu/menu';

@Component({
  selector: 'page-sender-recipient-form',
  templateUrl: 'sender-recipient-form.html'
})
export class SenderRecipientFormPage {

  submitAttempt: boolean = false;

  senderForm: any;
  recipientForm: any;
  public disabled: boolean = true;
  public thuOrSun: boolean = true;
  userId: any;
  deliveryId: any;
  day: any;
  /*pickupDate = {
    time: '1990-02-19'
  };*/
  // localDate = '2017-03-11';

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public server: Maanserver,
    public formBuilder: FormBuilder) {

    this.userId = navParams.get('userId');
    this.deliveryId = navParams.get('deliveryId');

    this.senderForm = formBuilder.group({
      // customerNumber: ['', Validators.compose([Validators.pattern('[0-9]*')])],
      trackingPlace: ['', Validators.required],
      phoneNumber: ['', Validators.compose([Validators.maxLength(12), Validators.pattern('[0-9]*'), Validators.required])],
      // pickupDate: [''],
      // dropOffDate: ['', Validators.required]
    });

    this.recipientForm = formBuilder.group({
      firstName: ['', Validators.compose([Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      lastName: ['', Validators.compose([Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      address: ['', Validators.required],
      phoneNumber: ['', Validators.compose([Validators.maxLength(12), Validators.pattern('[0-9]*'), Validators.required])],
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SenderRecipientFormPage');
  }
  setDate(e) {
    console.log(e);
  }

  enableCheck(e) {
    console.log(e);
    if (e.checked === true) {
      this.disabled = false;
    }
    else {
      this.disabled = true;
    }
  }
  termsAndConditions() {
    let modal = this.modalCtrl.create(TermsConditionsPage);
    modal.present();
  }

  submitSenderRecipientData() {
    this.submitAttempt = true;
    if (!this.senderForm.valid) {
      console.log(' Some values were not given or were incorrect, please fill them');
    } else {
      console.log('success');
      console.log(this.senderForm.value);
      console.log(this.recipientForm.value);
      let response = this.server
        .sendingDeliveryData(this.senderForm.value,
        this.recipientForm.value, this.userId, this.deliveryId, this.day);

      let loading = this.loadingCtrl.create({
        content: "Please wait...",
      });
      loading.present();
      response.subscribe(res => {
        console.log('response from maan server');
        console.log(res);
        if (res.success === true) {
          let alert = this.alertCtrl.create({
            title: 'Success!',
            subTitle: res.message,
            buttons: ['Ok']
          });
          alert.present();
          loading.dismiss();
          this.navCtrl.setRoot(MenuPage);
        } else {
          let alert = this.alertCtrl.create({
            title: 'Failure!',
            subTitle: res.message,
            buttons: ['Retry']
          });
          alert.present();
          loading.dismiss();
        }
      });
    }
  }
  getDate() {
    DatePicker.show({
      date: new Date(),
      mode: 'date'
    }).then(data => {
      this.day = data;
      let day = data.getDay();
      if(day == 0 ||  day == 4) {
        console.log('success');
        this.thuOrSun = false;
      } else {
        console.log('please select again');
        let alert = this.alertCtrl.create({
            title: 'Pickup date!',
            subTitle: 'please select either thursday or sunday',
            buttons: ['Retry']
          });
          alert.present();
          this.thuOrSun = true;
      }
    });
    /*.then(
      date => console.log('Got Date: ', date),
      err => console.log('Error occured while getting date: ', err)
    );*/
  }
}
