import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { CallNumber } from 'ionic-native';
import { InAppBrowser, SocialSharing } from 'ionic-native';

declare var cordova: any;

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactPage');
  }
  call() {
    CallNumber.callNumber('+37068347118', true)
      .then(() => console.log('Launched dialer!'))
      .catch(() => console.log('Error launching dialer'));
  }
  openBrowser() {
    // let browser = new InAppBrowser('https://ionic.io', '_system');
    cordova.InAppBrowser.open('http://www.vezejas.com', '_blank', 'location=yes');
    // browser.show();
    // browser.
    // browser.close();
  }
  openFb() {
    // let browser = new InAppBrowser('https://www.facebook.com/radijas', '_system');
    // browser.close();
    cordova.InAppBrowser.open('https://www.facebook.com/vezejascom', '_blank', 'location=yes');
  }
  openEmail() {
    // cordova.InAppBrowser.open('https://www.facebook.com/radijas','_blank', 'location=yes');
    SocialSharing.shareViaEmail('Body', 'Subject', ['info@vezejas.com']).then(() => {
      console.log('opened email');
    }).catch((err) => {
      console.log('an error happend');
      console.log(err);
    });
  }
}
