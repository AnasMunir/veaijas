import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { AnimalTransportFormPage } from '../animal-transport-form/animal-transport-form';
import { OutsideCargoPagePage } from '../outside-cargo-page/outside-cargo-page';
import { StandardPackageFormPage } from '../standard-package-form/standard-package-form';
import { RemovalsFormPage } from '../removals-form/removals-form';
import { CarTransportFormPage } from '../car-transport-form/car-transport-form';

@Component({
  selector: 'page-place-order',
  templateUrl: 'place-order.html'
})
export class PlaceOrderPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad PlaceOrderPage');
  }

  goToAnimalTransportPage() {
    this.navCtrl.push(AnimalTransportFormPage);
  }

  goToOutsideCargoPage() {
    this.navCtrl.push(OutsideCargoPagePage);
  }

  goToStandardPackagePage() {
    this.navCtrl.push(StandardPackageFormPage);
  }
  goToRemovalsForm() {
    this.navCtrl.push(RemovalsFormPage);
  }
  goToTransportPage() {
    this.navCtrl.push(CarTransportFormPage);
  }

}
