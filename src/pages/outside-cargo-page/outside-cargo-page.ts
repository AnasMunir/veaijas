import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { Camera, File, Transfer, NativeStorage } from 'ionic-native';

import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Maanserver } from '../../providers/maanserver';

import { SenderRecipientFormPage } from '../sender-recipient-form/sender-recipient-form';

declare var cordova: any;

@Component({
  selector: 'page-outside-cargo-page',
  templateUrl: 'outside-cargo-page.html'
})
export class OutsideCargoPagePage {

  submitAttempt: boolean = false;
  cargoForm: any;
  photoRef: any;
  imageChosen: any = 0;
  imagePath = '';
  imageNewPath: any;
  postTitle: any;
  desc: any;
  userId: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public server: Maanserver,
    public formBuilder: FormBuilder) {

    this.cargoForm = formBuilder.group({
      length: ['', Validators.compose([Validators.required])],
      width: ['', Validators.compose([Validators.required])],
      height: ['', Validators.compose([Validators.required])],
      weight: ['', Validators.compose([Validators.required])],
      moreInfo: ['', Validators.compose([Validators.pattern('[a-zA-Z0-9 ]*')])]
    });
    NativeStorage.getItem('user').then(data => {
      this.userId = data.userId;
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OutsideCargoPagePage');
  }

  openCamera() {
    console.log('gallery clicked');
    console.log('camera clicked');
    Camera.getPicture({
      quality: 95,
      destinationType: Camera.DestinationType.FILE_URI,
      sourceType: Camera.PictureSourceType.CAMERA,
      allowEdit: false,
      targetWidth: 800,
      targetHeight: 800,
      saveToPhotoAlbum: true
    }).then(imageData => {
      this.photoRef = imageData;
      console.log('imageData'); console.log(imageData);
      var sourceDirectory = imageData.substring(0, imageData.lastIndexOf('/') + 1);
      var sourceFileName = imageData.substring(imageData.lastIndexOf('/') + 1, imageData.length);
      sourceFileName = sourceFileName.split('?').shift();
      File.copyFile(sourceDirectory, sourceFileName, cordova.file.externalApplicationStorageDirectory, sourceFileName)
        .then((result: any) => {
          console.log('result'); console.log(result);
          this.imagePath = imageData;
          this.imageChosen = 1;
          this.imageNewPath = result.nativeURL;
        }).catch(err => console.log(err));

    }).catch(err => console.log);
  }

  openGallery() {
    console.log('gallery clicked');
    console.log('camera clicked');
    Camera.getPicture({
      quality: 95,
      destinationType: Camera.DestinationType.FILE_URI,
      sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
      allowEdit: false,
      targetWidth: 800,
      targetHeight: 800,
      saveToPhotoAlbum: true
    }).then(imageData => {
      this.photoRef = imageData;
      console.log('imageData'); console.log(imageData);
      var sourceDirectory = imageData.substring(0, imageData.lastIndexOf('/') + 1);
      var sourceFileName = imageData.substring(imageData.lastIndexOf('/') + 1, imageData.length);
      sourceFileName = sourceFileName.split('?').shift();
      File.copyFile(sourceDirectory, sourceFileName, cordova.file.externalApplicationStorageDirectory, sourceFileName)
        .then((result: any) => {
          console.log('result'); console.log(result);
          this.imagePath = imageData;
          this.imageChosen = 1;
          this.imageNewPath = result.nativeURL;
        }).catch(err => console.log(err));

    }).catch(err => console.log);
  }

  submitOutsideCargoData() {
    this.submitAttempt = true;
    if (!this.cargoForm.valid) {
      console.log(' Some values were not given or were incorrect, please fill them');
      console.log(this.cargoForm.value);
    } else {
      console.log('success');
      console.log(this.cargoForm.value);
      let response = this.server.sendingFormsData(this.cargoForm.value, 'cargo', this.userId);
      let loading = this.loadingCtrl.create({
        content: "Please wait...",
        dismissOnPageChange: true
      });
      loading.present();
      response.subscribe(res => {
        console.log('response from maan server');
        console.log(res);
        if (res.success === true) {
          if (this.photoRef) {

            let id = res.deliveryId;
            let filename = this.imagePath.split('/').pop();
            console.log('filename'); console.log(filename);
            let options = {
              fileKey: "file",
              fileName: filename,
              chunkedMode: false,
              mimeType: "image/jpg",
              params: { 'title': this.postTitle, 'description': this.desc }
            };

            const fileTransfer = new Transfer();

            fileTransfer.upload(this.imageNewPath,
              'http://tvtime.pk/vezejas/data/image?image=' + res.image + '&userId=' + this.userId + '&id=' + id,
              options).then((entry) => {
                console.log('entry');
                console.log(this.imageNewPath);
                console.log(entry);

                this.imagePath = '';
                this.imageChosen = 0;
                // loader.dismiss();
              }).then(_ => {
                loading.dismiss();
              })
              .catch(console.log);
          }

          let alert = this.alertCtrl.create({
            title: 'Success!',
            subTitle: res.message,
            buttons: ['OK']
          });
          alert.present();
          this.navCtrl.push(SenderRecipientFormPage, {
            userId: this.userId,
            deliveryId: res.deliveryId
          });
        } else {
          let alert = this.alertCtrl.create({
            title: 'Failure!',
            subTitle: res.message,
            buttons: ['Retry']
          });
          alert.present();
          loading.dismiss();
        }
      });
    }
  }

}
