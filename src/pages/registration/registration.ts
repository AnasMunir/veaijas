import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, LoadingController, AlertController } from 'ionic-angular';
import { NativeStorage } from 'ionic-native';
// for form validation
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { EmailValidator } from '../../validators/email';

import { TermsConditionsPage } from '../terms-conditions-page/terms-conditions-page';
import { MenuPage } from '../menu/menu';

import { Maanserver } from '../../providers/maanserver';

@Component({
  selector: 'page-registration',
  templateUrl: 'registration.html'
})
export class RegistrationPage {

  submitAttempt: boolean = false;

  individualRegistrationForm: any;
  companyRegistrationForm: any;
  registrationTypeForm: any;
  registrationType: any;

  public individualCheckDisabled: boolean = true;
  public companyCheckDisabled: boolean = true;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public modalCtrl: ModalController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public server: Maanserver) {

    this.registrationTypeForm = new FormGroup({
      "registrationType": new FormControl({ value: 'individual', disabled: false })
    });

    this.individualRegistrationForm = formBuilder.group({
      // name, lastname, email, phone, address
      firstName: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      lastName: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      phone: ['', Validators.compose([Validators.maxLength(12), Validators.pattern('[0-9]*'), Validators.required])],
      address: ['', Validators.required],
      email: ['', Validators.compose([Validators.required, EmailValidator.isValid])],
      password: ['', Validators.compose([Validators.minLength(6), Validators.required])]
    });

    // companyName, companyCode, mva code, address, phone, email
    this.companyRegistrationForm = formBuilder.group({
      companyName: ['', Validators.compose([Validators.pattern('[0-9a-zA-Z ]*'), Validators.required])],
      companyCode: ['', Validators.required],
      mvaCode: ['', Validators.compose([Validators.pattern('[0-9a-zA-Z ]*'), Validators.required])],
      address: ['', Validators.required],
      phone: ['', Validators.compose([Validators.maxLength(12), Validators.pattern('[0-9]*'), Validators.required])],
      email: ['', Validators.compose([Validators.required, EmailValidator.isValid])],
      password: ['', Validators.compose([Validators.minLength(6), Validators.required])]
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegistrationPage');
  }
  doSubmit(event) {
    console.log('Submitting form', this.registrationTypeForm.value);
    event.preventDefault();
  }
  showRadioValue() {
    // console.log(this.registrationType);
    console.log(this.registrationTypeForm.value);
  }

  enableCheckIndividual(e) {
    console.log(e);
    if (e.checked === true) {
      this.individualCheckDisabled = false;
    }
    else {
      this.individualCheckDisabled = true;
    }
  }

  enableCheckCompany(e) {
    console.log(e);
    if (e.checked === true) {
      this.companyCheckDisabled = false;
    }
    else {
      this.companyCheckDisabled = true;
    }
  }

  termsAndConditions() {
    let modal = this.modalCtrl.create(TermsConditionsPage);
    modal.present();
  }

  submitIndividualForm() {

    this.submitAttempt = true;
    if (!this.individualRegistrationForm.valid) {
      console.log(' Some values were not given or were incorrect, please fill them');
    } else {
      console.log('success!');
      console.log(this.individualRegistrationForm.value);
      let response = this.server
        .sendIndividualDataToServer(
        this.individualRegistrationForm.value, this.registrationTypeForm.value.registrationType);
      let loading = this.loadingCtrl.create({
        content: "Please wait...",
        dismissOnPageChange: true
      });
      loading.present();
      response.subscribe(res => {
        console.log('response from maan server');
        console.log(res);
        if (res.success === true) {
          console.log(res);
          let userId = res.userId;
          let authToken = res.authToken;
          /*let email = res.email;
          let firstName = res.firstName;
          let lastName = res.lastName;
          let authToken = res.authToken;
          let userId = res.userId;
          let message = res.message;*/
          NativeStorage.setItem('user',
            {
              userId: userId,
              authToken: authToken,
              userType: this.registrationTypeForm.value.registrationType,
              firstName: this.individualRegistrationForm.value.firstName,
              lastName: this.individualRegistrationForm.value.lastName,
              phone: this.individualRegistrationForm.value.phone,
              address: this.individualRegistrationForm.value.address,
              email: this.individualRegistrationForm.value.email,

            }).catch((err) => { console.log(err) });
            
          let alert = this.alertCtrl.create({
            title: 'Success!',
            subTitle: 'successfully registered',
            buttons: ['Ok']
          });
          alert.present();
          loading.dismiss();
          this.navCtrl.push(MenuPage);
        } else {
          let alert = this.alertCtrl.create({
            title: 'Failure!',
            subTitle: res.message,
            buttons: ['Retry']
          });
          alert.present();
          loading.dismiss();
        }
      });

    }
  }

  submitCompanyForm() {
    this.submitAttempt = true;
    if (!this.companyRegistrationForm.valid) {
      console.log(' Some values were not given or were incorrect, please fill them');
    } else {
      console.log('success!');
      console.log(this.companyRegistrationForm.value);
      let response = this.server
        .sendIndividualDataToServer(
        this.companyRegistrationForm.value, this.registrationTypeForm.value.registrationType);
      let loading = this.loadingCtrl.create({
        content: "Please wait...",
        dismissOnPageChange: true
      });
      loading.present();
      response.subscribe(res => {
        console.log('response from maan server');
        console.log(res);
        if (res.success === true) {
          console.log(res);
          let userId = res.userId;
          let authToken = res.authToken;
          NativeStorage.setItem('user',
            {
              userId: userId,
              authToken: authToken,
              userType: this.registrationTypeForm.value.registrationType,
              companyName: this.companyRegistrationForm.value.companyName,
              companyCode: this.companyRegistrationForm.value.companyCode,
              mvaCode: this.companyRegistrationForm.value.mvaCode,
              address: this.companyRegistrationForm.value.address,
              phone: this.companyRegistrationForm.value.phone,
              email: this.companyRegistrationForm.value.email,
            }).catch((err) => { console.log(err) });

          let alert = this.alertCtrl.create({
            title: 'Success!',
            subTitle: 'successfully registered',
            buttons: ['Ok']
          });
          alert.present();
          loading.dismiss();
          this.navCtrl.push(MenuPage);
        } else {
          let alert = this.alertCtrl.create({
            title: 'Failure!',
            subTitle: res.message,
            buttons: ['Retry']
          });
          alert.present();
          loading.dismiss();
        }
      });

    }
  }

}
