import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { NativeStorage } from 'ionic-native';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { MenuPage } from '../menu/menu';

import { Maanserver } from '../../providers/maanserver';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  submitAttempt: boolean = false;
  loginForm: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public formBuilder: FormBuilder,
    public server: Maanserver) {

    this.loginForm = formBuilder.group({
      id: ['', Validators.compose([Validators.required, Validators.pattern('[0-9]*')])],
      password: ['', Validators.compose([Validators.minLength(6), Validators.required])]
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  goToMenu() {
    this.navCtrl.push(MenuPage);
  }

  submitloginForm() {

    this.submitAttempt = true;
    if (!this.loginForm.valid) {
      console.log(' Some values were not given or were incorrect, please fill them');
    } else {
      console.log('success!');
      console.log(this.loginForm.value);
      let response = this.server.loginDataToServer(this.loginForm.value);
      let loading = this.loadingCtrl.create({
        content: "Please wait...",
        dismissOnPageChange: true
      });
      loading.present();
      response.subscribe(res => {
        console.log('response from maan server');
        console.log(res);
        if (res.success === true) {
          let authToken = res.authToken;
          let userId = res.userId;
          let message = res.message;
          NativeStorage.setItem('user',
            {
              userId: res.userId,
              authToken: res.authToken,
            }).catch((err) => { console.log(err) });
          this.navCtrl.push(MenuPage);
        } else {
          let alert = this.alertCtrl.create({
            title: 'Failure!',
            subTitle: res.message,
            buttons: ['Retry']
          });
          alert.present();
          loading.dismiss();
        }
      });

    }
  }

}
