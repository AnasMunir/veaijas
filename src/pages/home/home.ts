import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';
import { RegistrationPage } from '../registration/registration';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }
  goToRegistration() {
    this.navCtrl.push(RegistrationPage);
  }
  goToLogin() {
    this.navCtrl.push(LoginPage);
  }
  
}
