import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';


@Component({
  selector: 'page-news-modal',
  templateUrl: 'news-modal.html'
})
export class NewsModalPage {
  title: any;
  description: any;

  constructor(public navCtrl: NavController,
  public navParams: NavParams,
  public viewCtrl: ViewController) {

    this.title = navParams.get('title');
    this.description = navParams.get('description');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NewsModalPage');
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }

}
