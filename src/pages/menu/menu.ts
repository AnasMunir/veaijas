import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { ContactPage } from '../contact/contact';
import { PlaceOrderPage } from '../place-order/place-order';
import { NewsPage } from '../news/news';

declare var cordova: any;

@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html'
})
export class MenuPage {

  // menuItems = []
  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad MenuPage');
  }

  goToContactPage() {
    this.navCtrl.push(ContactPage);
  }
  goToPlaceOrderPage() {
    this.navCtrl.push(PlaceOrderPage);
  }
  goToNews() {
    this.navCtrl.push(NewsPage);
  }
  openFb() {
    // let browser = new InAppBrowser('https://www.facebook.com/radijas', '_system');
    // browser.close();
    cordova.InAppBrowser.open('https://www.facebook.com/vezejascom','_blank', 'location=yes');
  }

}
