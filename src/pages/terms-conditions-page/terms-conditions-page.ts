import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-terms-conditions-page',
  templateUrl: 'terms-conditions-page.html'
})
export class TermsConditionsPage {

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad TermsConditionsPagePage');
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }
}
