import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';

import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Maanserver } from '../../providers/maanserver';
import { NativeStorage } from 'ionic-native';
import { SenderRecipientFormPage } from '../sender-recipient-form/sender-recipient-form';

@Component({
  selector: 'page-animal-transport-form',
  templateUrl: 'animal-transport-form.html'
})
export class AnimalTransportFormPage {

  submitAttempt: boolean = false;

  animalTypeForm: any;
  animalType: any;
  dogTransportForm: any;
  catTransportForm: any;
  otherAnimalTransportForm: any;
  userId: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public server: Maanserver,
    public formBuilder: FormBuilder) {

    this.animalTypeForm = new FormGroup({
      "animalType": new FormControl({ value: 'dog', disabled: false })
    });

    this.dogTransportForm = formBuilder.group({
      months: ['', Validators.required],
      years: ['', Validators.required],
      breed: ['', Validators.compose([Validators.pattern('[a-zA-Z ]*')])],
      moreInfo: ['']
    });

    this.catTransportForm = formBuilder.group({
      months: ['', Validators.required],
      years: ['', Validators.required],
      breed: ['', Validators.compose([Validators.pattern('[a-zA-Z ]*')])],
      moreInfo: ['']
    });

    this.otherAnimalTransportForm = formBuilder.group({
      moreInfo: ['', Validators.required]
    });

    NativeStorage.getItem('user').then(data => {
      this.userId = data.userId;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AnimalTransportFormPage');
  }

  next() {
    this.submitAttempt = true;
    this.navCtrl.push(SenderRecipientFormPage);
  }
  submitDogData() {
    this.submitAttempt = true;
    if (!this.dogTransportForm.valid) {
      console.log('Some values were not given or were incorrect, please fill them');
    } else {
      console.log('success');
      console.log(this.dogTransportForm.value);
      let response = this.server.sendingFormsData(this.dogTransportForm.value, 'animal', this.userId);
      let loading = this.loadingCtrl.create({
        content: "Please wait...",
        dismissOnPageChange: true
      });
      loading.present();
      response.subscribe(res => {
        console.log('response from maan server');
        console.log(res);
        if (res.success === true) {
          this.navCtrl.push(SenderRecipientFormPage, {
            userId: this.userId,
            deliveryId: res.deliveryId
          });
        } else {
          let alert = this.alertCtrl.create({
            title: 'Failure!',
            subTitle: res.message,
            buttons: ['Retry']
          });
          alert.present();
          loading.dismiss();
        }
      });
    }
  }
  submitCatData() {
    this.submitAttempt = true;
    if (!this.catTransportForm.valid) {
      console.log('Some values were not given or were incorrect, please fill them');
    } else {
      console.log('success');
      console.log(this.catTransportForm.value);
      let response = this.server.sendingFormsData(this.catTransportForm.value, 'animal', this.userId);
      let loading = this.loadingCtrl.create({
        content: "Please wait...",
        dismissOnPageChange: true
      });
      loading.present();
      response.subscribe(res => {
        console.log('response from maan server');
        console.log(res);
        if (res.success === true) {

          this.navCtrl.push(SenderRecipientFormPage, {
            userId: this.userId,
            deliveryId: res.deliveryId
          });
        } else {
          let alert = this.alertCtrl.create({
            title: 'Failure!',
            subTitle: res.message,
            buttons: ['Retry']
          });
          alert.present();
          loading.dismiss();
        }
      });
    }
  }
  submitOtherAnimalData() {
    this.submitAttempt = true;
    if (!this.otherAnimalTransportForm.valid) {
      console.log('Some values were not given or were incorrect, please fill them');
    } else {
      console.log('success');
      console.log(this.otherAnimalTransportForm.value);
      let response = this.server.sendingFormsData(this.otherAnimalTransportForm.value, 'animal', this.userId);
      let loading = this.loadingCtrl.create({
        content: "Please wait...",
        dismissOnPageChange: true
      });
      loading.present();
      response.subscribe(res => {
        console.log('response from maan server');
        console.log(res);
        if (res.success === true) {
          this.navCtrl.push(SenderRecipientFormPage, {
            userId: this.userId,
            deliveryId: res.deliveryId
          });
        } else {
          let alert = this.alertCtrl.create({
            title: 'Failure!',
            subTitle: res.message,
            buttons: ['Retry']
          });
          alert.present();
          loading.dismiss();
        }
      });
    }
  }

}
