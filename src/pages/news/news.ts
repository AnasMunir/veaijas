import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { Maanserver } from '../../providers/maanserver';
import { NewsModalPage } from '../news-modal/news-modal';
// import { TruncatePipe } from '../../pipes/truncate';

@Component({
  selector: 'page-news',
  templateUrl: 'news.html'
})
export class NewsPage {
  // img: any; title: any; description: any;
  newsData: any;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public server: Maanserver) {

      this.getNewsData();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NewsPage');
  }
  getNewsData() {
    let response = this.server.getNewsData()
    response.subscribe(res => {
      console.log('resonse from server');
      console.log(res);
      this.newsData = res;
    })
    console.log(response);
  }
  seeMore(title?, description?) {
    this.navCtrl.push(NewsModalPage, {
      title: title,
      description: description
    })
  }

}
