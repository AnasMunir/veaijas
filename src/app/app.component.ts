import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';
import { NativeStorage } from 'ionic-native';
import { HomePage } from '../pages/home/home';
// import { SenderRecipientFormPage } from '../pages/sender-recipient-form/sender-recipient-form'
// import { TruncatePipe } from '../pipes/truncate';
// import { LoginOrRegisterPage } from '../pages/login-or-register/login-or-register';
// import { RegistrationPage } from '../pages/registration/registration';
import { MenuPage } from '../pages/menu/menu';


@Component({
  templateUrl: 'app.html'
  // pipes: [TruncatePipe]
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage = HomePage;

  constructor(platform: Platform) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      let env = this;
      NativeStorage.getItem('user')
        .then(data => {
          // user is previously logged and we have his data
          // we will let him access the app
          console.log(data);
          env.nav.setRoot(MenuPage);
          Splashscreen.hide();
        },error => {
          //we don't have the user data so we will ask him to log in
          env.nav.setRoot(HomePage);
        }).catch(err => { console.log(err) });
      StatusBar.styleDefault();
      Splashscreen.hide();
    });
  }
}
