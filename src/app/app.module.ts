import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { RegistrationPage } from '../pages/registration/registration';
import { MenuPage } from '../pages/menu/menu';
import { TermsConditionsPage } from '../pages/terms-conditions-page/terms-conditions-page';
import { ContactPage } from '../pages/contact/contact';
import { PlaceOrderPage } from '../pages/place-order/place-order';
import { SenderRecipientFormPage } from '../pages/sender-recipient-form/sender-recipient-form';
import { AnimalTransportFormPage } from '../pages/animal-transport-form/animal-transport-form';
import { StandardPackageFormPage } from '../pages/standard-package-form/standard-package-form';
import { OutsideCargoPagePage } from '../pages/outside-cargo-page/outside-cargo-page';
import { RemovalsFormPage } from '../pages/removals-form/removals-form';
import { CarTransportFormPage } from '../pages/car-transport-form/car-transport-form';
import { LoginPage } from '../pages/login/login';
import { NewsPage } from '../pages/news/news';
import { NewsModalPage } from '../pages/news-modal/news-modal';

import { Maanserver } from '../providers/maanserver';
import { Truncate } from '../pipes/truncate';
import { DatePickerModule } from 'datepicker-ionic2';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    RegistrationPage,
    LoginPage,
    MenuPage,
    TermsConditionsPage,
    ContactPage,
    PlaceOrderPage,
    SenderRecipientFormPage,
    AnimalTransportFormPage,
    StandardPackageFormPage,
    OutsideCargoPagePage,
    RemovalsFormPage,
    CarTransportFormPage,
    NewsPage,
    NewsModalPage,
    Truncate
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    DatePickerModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    RegistrationPage,
    LoginPage,
    MenuPage,
    TermsConditionsPage,
    ContactPage,
    PlaceOrderPage,
    SenderRecipientFormPage,
    AnimalTransportFormPage,
    StandardPackageFormPage,
    OutsideCargoPagePage,
    RemovalsFormPage,
    CarTransportFormPage,
    NewsPage,
    NewsModalPage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}, Maanserver]
})
export class AppModule {}
